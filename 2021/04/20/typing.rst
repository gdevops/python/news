

========================================================================
2021-04-20 IMPORTANT: PEP 563, PEP 649 and the future of pydantic #2678
========================================================================

.. seealso::

   - https://github.com/samuelcolvin/pydantic/issues/2678
   - https://mail.python.org/archives/list/python-dev@python.org/thread/CLVXXPQ2T2LQ5MP2Y53VVQFCXYWQJHKZ/



.. figure:: tiangolo.png
   :align: center



PEP 563 and Python 3.10
=========================

.. seealso::

   - https://mail.python.org/archives/list/python-dev@python.org/thread/CLVXXPQ2T2LQ5MP2Y53VVQFCXYWQJHKZ/

This is amazing news!

On behalf of the FastAPI/pydantic communities, thanks to the Steering
Council, and everyone involved!

I understand the big effort and commitment the Steering Council is making
with this decision to support the pydantic and FastAPI communities
(especially with our short notice), and the last-minute extra work involved
in reverting the default for 3.10.

This will give us all the time to figure out how to handle it taking into
account pydantic/FastAPI and the use cases that would benefit from PEP 563
and PEP 649.

Thanks!

Sebastián
