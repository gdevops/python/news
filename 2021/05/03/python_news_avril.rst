
.. _python_news_2021_05_03:

=========================================================
**Python News: What's New From April 2021?**
=========================================================

- https://realpython.com/python-news-april-2021/


Python 3.10 Will Have Improved Error Messages
=================================================

- https://docs.python.org/3.10/whatsnew/3.10.html#better-error-messages

On April 9, 2021, Python core developer Pablo Galindo, who is also the
release manager for Python 3.10 and 3.11, tweeted a question directed
toward Python educators:


Overall it appears that the improved error messages are a big improvement
to Python’s user experience. For a complete list of error message improvements,
check out the What’s New in Python 3.10 page in the Python docs.


What PEP 563 Proposed to Improve Type Annotations
=====================================================

In September 2017, PEP 563 proposed changing from evaluating annotations
at function definition time to preserving them in string form in the
built-in __annotations__ dictionary.
This effectively solves the forward reference issue and eliminates the
computational overhead from importing typed modules.

Starting with Python 3.7, you can access this new behavior by importing
annotations from __future__.

This lets you use a Number type hint even if it hasn’t been defined:


In other words, tools can no longer expect type hints to be evaluated
for them and need to update their code to evaluate the annotations
explicitly as required.

With PEP 563 accepted and plans in place to change the default behavior,
projects like FastAPI and pydantic, which use annotations at runtime,
set out to support the PEP.
