

=============================================
2021-05-22 The 2021 Python Language Summit
=============================================

- https://pyfound.blogspot.com/2021/05/the-2021-python-language-summit.html


Every year, a small group of core developers from Python implementations
such as CPython, PyPy, Jython, and more come together to share information,
discuss problems, and seek consensus in order to help Python continue
to flourish.

The Python Language Summit features short presentations followed by
group discussions.

The topics can relate to the language itself, the standard library, the
development process, documentation, packaging, and more!

In 2021, the summit was held over two days by videoconference and was
led by Mariatta Wijaya and Łukasz Langa.

If you weren't able to attend the summit, then you can still stay up to
date with what's happening in the world of Python by reading blog posts
about all of the talks that were given.

Over the next few weeks, you'll be able to dive into all of the news
from the summit so you can join in on the big conversations that are
happening in the Python community.
