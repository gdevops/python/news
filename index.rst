
.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/python/news/rss.xml>`_

.. _python_news:

=====================
**Python** news
=====================

- https://rstockm.github.io/mastowall/?hashtags=python&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=python&server=https://fosstodon.org

.. toctree::
   :maxdepth: 5

   2022/2022
   2021/2021
   2020/2020
   1994/1994
