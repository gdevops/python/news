.. index::
   pair: Python ; PostgreSQL


.. _postgresql_2022_06_28:

==============================================
2022-06-28 **pip install postgresql-wheel**
==============================================

- https://github.com/michelp/postgresql-wheel


postgresql-wheel
======================

- https://github.com/michelp/postgresql-wheel/blob/main/README.md


A Python wheel for Linux containing a complete, self-contained,
locally installable PostgreSQL database server.

All servers run as the Python process user in a local path, so this
wheel does not require root or sudo privledges.  Databases can be
initialized in any directory.

Servers can be setup and torn down in test fixtures with no additional
outside dependencies.

Currently this wheel only works for most flavors of Linux.

Postgres is compiled in the same "manylinux" environments provided by
the `cibuildwheel <https://cibuildwheel.readthedocs.io/en/stable/>`_
tool using `Github Actions <https://github.com/michelp/postgresql-wheel/blob/main/.github/workflows/wheels.yml>`_
and directly archived into the wheel's "package_data".

The wheel can be installed with pip::


    $ pip install postgresql-wheel
