
.. _python_nodejs_2022_06_17:

=======================================
2022-06-17 **pip install nodejs-bin**
=======================================

- https://github.com/samwillis/nodejs-pypi

Introduction
============

Node.js is an open-source, cross-platform, back-end JavaScript runtime
environment that runs on the V8 engine and executes JavaScript code
outside a web browser.

The nodejs-bin Python package redistributes Node.js so that it can be
used as a dependency of Python projects.

With nodejs-bin you can call nodejs, npm and npx from both the command
line and a Python API.

Note: this is an unofficial Node.js distribution.

This is intended for use within Python virtual environments and containers,
it should probably not be used for global instillation


Inspired by @simonw and #ziglang, you can now (alpha!) install @nodejs
in a Python virtual environment with just:

::

    pip install nodejs-bin

You can even make it an optional requirement for a package, makes it
super easy to install a full stack env


::

    pipx run --spec=nodejs-bin npx .
