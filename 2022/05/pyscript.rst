.. index::
   pair: Web ; pyscript

.. _pyscript_announce:

=========================================================================================================================
Python 3.11 in the web browser **pyscript** #python #web #wasm #pyodide #browser #emscripten #anaconda #experimentation
=========================================================================================================================


- https://engineering.anaconda.com/2022/04/welcome-pyscript.html
- https://github.com/pyscript/pyscript/blob/main/README.md
- https://github.com/pyscript/pyscript
- https://pyscript.net/examples/
- https://community.anaconda.cloud/c/tech-topics/pyscript
- https://www.pyscript.net/

Announce
============

- https://www.anaconda.com/blog/pyscript-python-in-the-browser


pyscript_dev pre-announce
============================

- https://x.com/pyscript_dev/status/1510475506663059460?s=20&t=U6GhHWwf4duT3SoTQNNm9Q


Avertissement
==============

- https://engineering.anaconda.com/2022/04/welcome-pyscript.html


Today, April 30th, 2022, PyScript is just at its beginning and is very
limited compared to the vision we have for the project.

It's a demonstration that we can build the vision and the technology is
mature enough for us to create a new way of programming, building,
sharing, and deploying applications.

Be advised that it's very unstable and limited, but it works and can be
used to hack with and build experimental applications.

We hope to make progress fast and that in a few weeks/months, this post
will be outdated :slightly_smiling_face:.



https://pyscript.net/examples/
==================================

- https://pyscript.net/examples/

Python 3.11 in the web browser
===================================

- https://speakerdeck.com/tiran/python-3-dot-11-in-the-web-browser-a-journey-pycon-de-2022-keynote


What is PyScript
===================

- https://github.com/pyscript/pyscript/blob/main/README.md

tl;dr
------

PyScript is a Pythonic alternative to Scratch, JSFiddle or other
"easy to use" programming frameworks, making the web a friendly, hackable,
place where anyone can author interesting and interactive applications.

To demonstrate pyscript, see [the pyscript folder](pyscriptjs/README.md).

Longer Version
----------------

PyScript is a meta project that aims to combine multiple open technologies
to create a framework for users to use Python (and other languages) to
create sophisticated applications in the browser.

It highly integrates with the way the DOM works in the browser and allows
users to add logic, in Python, in a way that feels natural to web as
well as Python developers.


Where is PyScript today ?
=============================

- https://engineering.anaconda.com/2022/04/welcome-pyscript.html

Today, April 30th, 2022, PyScript is just at its beginning and is very
limited compared to the vision we have for the project.

It's a demonstration that we can build the vision and the technology is
mature enough for us to create a new way of programming, building,
sharing, and deploying applications.

Be advised that it's very unstable and limited, but it works and can be
used to hack with and build experimental applications.

We hope to make progress fast and that in a few weeks/months, this post
will be outdated :).

For more information about the available features and how to get started,
visit the project documentation.

Thanks
=======

PyScript wouldn’t be here without the help of some incredible people.
We’d really like to thank:

Peter Wang, Kevin Goldsmith, Philipp Rudiger, Antonio Cuni, Russell Keith-Magee,
Mateusz Paprocki, Princiya Sequeira, Jannis Leidel, David Mason, Anna NG,
Maria Genovese, Katherine Kinnaman, Kent Pribbernow, Albert DeFusco,
Michael Verhulst and Chris Leonard for the contributions to the project
and helping spin it up

Especial thanks to the Pyodide maintainers
(Roman Yurchak, Hood Chatham and all the contributors)



Welcome to the world PyScript
================================

Source: https://engineering.anaconda.com/author/fabio-pliger.html

Sat 30 April 2022 By Fabio Pliger

One of the main reasons I joined Anaconda seven and a half years ago was
the company’s commitment to the data science and Python communities by
creating tools that enable people to do more with less.

Today I'm happy to announce a new project that we’ve been working on
here at Anaconda and we hope will take another serious step towards
making programming and data science available and accessible to everyone.

What is PyScript
-------------------

PyScript is a framework that allows users to run Python and create rich
applications in the browser by simply using special HTML tags provided
by the framework itself.

Core features include:

- Python in the browser: Enable drop-in content, external file hosting
  (made possible by the Pyodide project, thank you!), and application
  hosting without the reliance on server-side configuration
- Python ecosystem: Run many popular packages of Python and the scientific
  stack (such as numpy, pandas, scikit-learn, and more)
- Python with JavaScript: Bi-directional communication between Python
  and Javascript objects and namespaces
- Environment management: Allow users to define what packages and files
  to include for the page code to run
- Visual application development: Use readily available curated UI components,
  such as buttons, containers, text boxes, and more
- Flexible framework: A flexible framework that can be leveraged to create
  and share new pluggable and extensible components directly in Python

All that to say… PyScript is just HTML, only a bit (okay, maybe a lot)
more powerful, thanks to the rich and accessible ecosystem of Python libraries.


Try PyScript
===============

- https://github.com/pyscript/pyscript/blob/main/README.md

To try PyScript, import the pyscript to your html page with::


    <link rel="stylesheet" href="https://pyscript.net/alpha/pyscript.css" />
    <script defer src="https://pyscript.net/alpha/pyscript.js"></script>


At that point, you can then use PyScript components in your html page.
PyScript currently implements the following elements:

- `<py-script>`: that can be used to define python code that is executable
  within the web page. The element itself is not rendered to the page and
  only used to add logic
- `<py-repl>`: creates a REPL component that is rendered to the page as
  a code editor and allows users to write code that can be executed


PyScript: Python in the Web Browser
===================================

- https://ericmjl.github.io/blog/2022/5/1/pyscript-python-in-the-web-browser/

Reflections
----------------

I'm not sure that 2022 is going to be the year of the Linux desktop, but
I'm very sure that 2022 is already the year of Python embedded in the browser.

I wrote this blog post entirely in Markdown, with only a few <div></div>,
<pre></pre> and <py-script></py-script> tags sprinkled throughout the
post to make this happen.

As someone who has programmed in Python for my entire professional life,
there's a glaring inability for me to embed Python in HTML documents.

As such, Python has a strong server-to-client vibe baked into the ecosystem.

PyScript changes that paradigm: everything you saw above was rendered
client-side only. My website is a static site, and yet you were able to
run an embedded Python REPL here!

The possibility of using Python just like JavaScript is pretty amazing.

I can see many possibilities going forward, both at work, in education,
and more. Kudos, Peter, Fabio, and the rest of the Anaconda team that
put this out. I can't wait to see how PyScript evolves!
