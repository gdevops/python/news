
.. _bundling_binary_tools:

=========================================================================================================================
**Bundling binary tools in Python wheels**
=========================================================================================================================


- https://simonwillison.net/2022/May/23/bundling-binary-tools-in-python-wheels/
- https://www.dampfkraft.com/code/distributing-large-files-with-pypi.html

pip install ziglang
=====================

One of the options provided for installing Zig (and its bundled toolchain)
is to use pip:

::

    % pip install ziglang
    ...
    % python -m ziglang cc --help
    OVERVIEW: clang LLVM compiler

    USAGE: zig [options] file...

    OPTIONS:
      -###                    Print (but do not run) the commands to run for this compilation
      --amdgpu-arch-tool=<value>
                              Tool used for detecting AMD GPU arch in the system.
    ...

**This means you can now pip install a full C compiler for your current
platform !**


pip install a SQLite database
==============================

It’s not quite the same thing, since it’s not packaging an executable,
but the one project I have that fits this mould if you squint a little
is my datasette-basemap plugin.

It’s a Datasette plugin which bundles a 23MB SQLite database file containing
OpenStreetMap tiles for the first seven zoom levels of their world map—5,461 tile images total.

I built it so that people could use my datasette-cluster-map and
datasette-leaflet-geojson entirely standalone, without needing to load
tiles from a central tile server.


.. warning:: Update: Paul O’Leary McCann points out that PyPI has a
  default 60MB size limit for packages, though it can be raised on
  a case-by-case basis.

  He wrote about this in `Distributing Large Files with PyPI Packages <https://www.dampfkraft.com/code/distributing-large-files-with-pypi.html>`_
