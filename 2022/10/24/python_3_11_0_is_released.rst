
.. _python_3_11_0_2022_10_24:

==========================================
2022-10-24 **Python 3.11.0 is released**
==========================================

- https://www.youtube.com/watch?v=PGZPSWZSkJI

- :ref:`releasing_python_3_11_0`

.. figure:: logo_python_3_11_only_logo.png
   :align: center


Pablo announce on twitter
=============================

- https://x.com/pyblogsal/status/1584641320764993536?s=20&t=IbFgWpuya5M81Jrxgxd_kg

Python 3.11 is finally released.

In the CPython release team, we have put a lot of effort into making 3.11
the best version of Python possible. Better tracebacks, faster Python,
exception groups and except*, typing improvements and much more.

Get it here: https://www.python.org/downloads/release/python-3110/
