.. index::
   ! sigstore

.. _sigstore_2022_09_08:

===================================================================
The CPython release artifacts will be signed with sigstore
===================================================================


sigstore
==========

- https://docs.sigstore.dev/
- https://x.com/projectsigstore
- https://blog.sigstore.dev/

Python sigstore client
===========================


- https://github.com/sigstore/sigstore-python
- https://www.python.org/download/sigstore/
- https://www.redhat.com/en/blog/sigstore-open-answer-software-supply-chain-trust-and-security


Tweet
=====

- https://x.com/di_codes/status/1567946961386340352

Starting with the latest @thepsf Python releases (3.7.14, 3.8.14, 3.9.14,
3.10.7 and soon 3.11.0), the CPython release artifacts will be signed
w/ @projectsigstore by release managers @pyblogsal and @llanga! 🎉

More details on signature verification here: https://www.python.org/download/sigstore/


History
===========

- https://www.redhat.com/en/blog/sigstore-open-answer-software-supply-chain-trust-and-security

Everything starts from somewhere, and software is no different - just as
physical goods have a point of origin and an associated supply chain,
so does code.

In today’s world, the origin story for most software applications starts,
at least partially if not entirely, in an open source community.

So how do you secure a supply chain for a product that has no physical
form, no box to lock and is created in an environment where anyone can
contribute to it?

This is the goal of sigstore, an open source project originally conceived
and prototyped at Red Hat and now under the auspices of the Linux Foundation
with backing from Red Hat, Google and other IT leaders.

Sigstore offers a method to better secure software supply chains in an
open, transparent and accessible manner.

The need for a project like sigstore is underscored by the recent attacks
and breaches of critical digital infrastructure, both in North America
and globally, leading to a U.S. presidential executive order aimed at
furthering software supply chain security.

So why do we need something like sigstore, exactly? And what does sigstore
do that existing technologies, built to certify and sign digital technologies,
don’t?



Sigstore Information
========================

- https://www.python.org/download/sigstore/

Background

Starting with the Python 3.7.14, Python 3.8.14, Python 3.9.14, and
Python 3.10.7 releases, CPython release artifacts are additionally
signed with Sigstore (**in addition to existing GPG signatures**).

This page provides guidance on verifying Sigstore signatures as a
CPython consumer, and outlines some motivation for using these additional
signatures.


Sigstore verification of CPython Releases
===========================================

Introduction to Sigstore
----------------------------

**Sigstore is a new standard for signing, verifying and protecting software.**


The Sigstore project is a set of tools and services:

- a certificate authority
- a signature transparency log
- multiple ecosystem-specific signing clients (such as https://pypi.org/p/sigstore/)

At a high level, Sigstore uses a certificate authority to tie OpenID Connect
(OIDC) identities to ephemeral keys, and uses a transparency log to publish
the results of signing events.

This eliminates the need for signers to manage private keys.

It also allows users to verify signatures based on characteristics of
the OIDC identities, such as an email address.

More detail about the signing process and the interplay of these tools
and services is provided in the `Sigstore docs <https://docs.sigstore.dev/>`_.

Additionally, a security model for Sigstore can be found `here <https://docs.sigstore.dev/security>`_.
